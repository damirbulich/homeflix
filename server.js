require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

app.use(
    bodyParser.json({
        limit: "20mb",
    })
);
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(express.static(path.join(__dirname, "dist")));
app.use("/static", express.static(path.join(__dirname)));

const movieRouter = require("./app/routes/movie.route");
app.use("/movies", movieRouter);

const subRouter = require("./app/routes/subtitle.route");
app.use("/subtitles", subRouter);

const userRouter = require("./app/routes/user.route");
app.use("/users", userRouter);

const favRouter = require("./app/routes/favorite.route");
app.use("/favorites", favRouter);

app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "dist/index.html"));
});

let port = process.env.APP_PORT || 3000;

app.listen(port, () => {
    console.log(
        `Server started on port ${port}\n\nLink:\n    http://localhost:${port}\n`
    );
});