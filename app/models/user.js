/*
    Model za korisnike - sadrzi metode za CRUD operacije nad korisnicima u bazi
*/


const sql = require("../db.js");

// constructor
const User = function (user) {
    this.name = user.name;
};

User.create = (newUser, callback) => {
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("Error: " + err);
            callback(err, null);
            return;
        }
        // console.log("Created user: ", {
        //     id: res.insertId,
        //     ...newUser
        // });
        callback(null, {
            id: res.insertId,
            ...newUser
        });
    });
}

User.getAll = (where, callback) => {
    // console.log(where)
    if (!where || where.trim() == "") {
        sql.query("SELECT * FROM users", (err, res) => {
            if (err) {
                console.log("error: ", err);
                callback(null, err);
                return;
            }
            // console.log("users: ", JSON.parse(JSON.stringify(res)));
            callback(null, JSON.parse(JSON.stringify(res)));
        });
    } else {
        sql.query("SELECT * FROM users WHERE lower(name) LIKE ?", ["%" + where.toLowerCase() + "%"], (err, res) => {
            if (err) {
                console.log("error: ", err);
                callback(null, err);
                return;
            }
            // console.log("users: ", JSON.parse(JSON.stringify(res)));
            callback(null, JSON.parse(JSON.stringify(res)));
        });
    }
};


User.findById = (id, callback) => {
    sql.query("SELECT * FROM users WHERE id=?", [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            callback(null, err);
            return;
        }
        // console.log("users: ", JSON.parse(JSON.stringify(res)));
        callback(null, JSON.parse(JSON.stringify(res)));
    });
}

User.destroy = (id, callback) => {
    sql.query("DELETE FROM users WHERE id=?", [id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            callback(null, err);
            return;
        }
        // console.log("users: ", JSON.parse(JSON.stringify(res)));
        callback(null, JSON.parse(JSON.stringify(res)));
    });
}

module.exports = User;