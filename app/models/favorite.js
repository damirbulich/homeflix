const sql = require("../db");

const Favorite = function(favorite) {
    this.movie_id = favorite.movie_id;
    this.user_id = favorite.user_id;
};

Favorite.find = ([user_id, movie_id], callback) => {
    sql.query(
        "SELECT * FROM favorites WHERE user_id=? AND movie_id=?", [user_id, movie_id],
        (err, res) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            callback(null, JSON.parse(JSON.stringify(res)));
        }
    );
};

Favorite.create = (newFav, callback) => {
    sql.query("INSERT INTO favorites SET ?", newFav, (err, res) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        callback(null, { id: res.insertId, ...newFav });
    });
};

Favorite.getAll = (where, callback) => {
    sql.query(
        "SELECT * FROM favorites WHERE user_id=?", [where],
        (err, res) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            }
            callback(null, JSON.parse(JSON.stringify(res)));
        }
    );
};

Favorite.destroy = (id, callback) => {
    sql.query("DELETE FROM favorites WHERE id=?", id, (err, res) => {
        if (err) {
            console.log(err);
            callback(err, null);
            return;
        }
        callback(null, JSON.parse(JSON.stringify(res)));
    });
};

module.exports = Favorite;