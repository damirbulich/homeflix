# Homeflix

Kucni NETFLIX sustav koji se instalira na racunalo sa filmovima spojeno na lokalnu mrežu (npr. Raspberry PI).
Aplikacija koristi mysql bazu.
Web aplikacija se dohvaca web preglednikom na nacin da se unese:

1. ip adresa racunala na kojem je aplikacija te :(broj porta definiran u .env datoteci)
2. localhost:(broj porta) u slucaju da otvaramo aplikaciju na istom racunalu gdje je instalirana

### Postavljanje svih biblioteka o kojima aplikacija ovisi

```
npm install
```

### Postavljanje okruženja

Da bi smo postavili aplikaciju potrebno je definirati konekciju za bazu i port na koji će se pokretati aplikacija

```
cp .env.example .env
```

### Postvljanje baze

Potrebno je prvo kreirati lokalnu bazu i konekciju defnirati u .env datoteci. Sve tablice se generiraju automatski kada se pokrene naredba ispod

```
npm run migrate
```

### Kompajliranje i uzivo ucitavanje aplikacije (izrada i testiranje) - frontend

```
npm run serveDev
```

### Kompajliranje i sazimanje aplikacije za produkciju

```
npm run build
```

### Pokretanje servera za produkciju

```
npm run serve
```

ili (ako se mjenjao kod frontenda)

```
npm run build && npm run serve
```
